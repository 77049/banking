public interface BankService {

    List<Bank> getAllBanks();

    Bank getBankById(Long id);

    Bank createBank(Bank bank);

    Bank updateBank(Long id, Bank bank);

    void deleteBank(Long id);
}
